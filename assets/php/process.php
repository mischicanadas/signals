<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
    if(isset($_POST['Action']))
    {
        $Result = "";
        
        $Action = $_POST["Action"];
        
        switch($Action)
        {
            case "Signals": $Result = get_signals();break;
        }
        
        echo $Result;
    }

    function get_signals()
    {
        $Fecha = $_POST["Fecha"];
        $Row = "";

        switch($Fecha)
        {
            case "Now": $Query = "SELECT * FROM signals WHERE DATE(Fecha) = DATE(NOW()) ORDER BY iSignal DESC";break;
            case "All": $Query = "SELECT * FROM signals ORDER BY iSignal DESC";break;
            default: $Query = "SELECT * FROM signals WHERE DATE(Fecha) = '".$Fecha."' ORDER BY iSignal DESC";
        }

        $result = Ejecuta_Query($Query);
        while($Registro = $result->fetch_assoc())
        {
            $Row.="<tr>";
                $Row.="<td>".$Registro['iSignal']."</td>";
                $Row.="<td>".$Registro['Moneda']."</td>";
                $Row.="<td>".$Registro['Direccion']."</td>";
                $Row.="<td class='mobile-hide'>$".$Registro['PrecioEntrada']."</td>";
                $Row.="<td class='mobile-hide'>$".$Registro['PrecioExpiracion']."</td>";
                $Row.="<td class='mobile-hide'>".$Registro['Entrada']."</td>";
                $Row.="<td class='mobile-hide'>".$Registro['Expiracion']."</td>";
                $Row.="<td class='mobile-hide'>".$Registro['TimeFrame']."</td>";
                $Row.="<td>".$Registro['Estatus']."</td>";
                $Row.="<td class='mobile-hide'>".$Registro['Tipo']."</td>";
            $Row.="</tr>";
                
        }
    
        return $Row;
    }

    function run($Command)
    {
        $res = exec($Command);
        return $res;
    }

    function Ejecuta_Query ($Query)
    {
        include("ConnDB.php");
        
        //Abre una conexion a MySQL server
        $mysqli = new mysqli($ConnDB["Servidor"],$ConnDB["Usuario"],$ConnDB["Password"],$ConnDB["DB"]);

        //Arrojo cualquier error tipo connection error
        if ($mysqli->connect_error) {
            die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
        }
        $result = $mysqli->query($Query);
        //Cierro la conexion 
        $mysqli->close();
        
        return $result;
    }
?>