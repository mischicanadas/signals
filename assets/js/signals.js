$(document).ready(function(){

    //var _Table_Signals = $('#table_signals').DataTable({"ajax":"assets/data/signals.json"});
    //var _Fecha_Signal = "Now";
    var _Fecha_Signal = "All";
    get_signals(_Fecha_Signal);

    /*
    $.recur(60000, 'Signal Queue').progress(function(name){
        get_signals(_Fecha_Signal);
        console.log(name+' update : '+  new Date().toLocaleString());
    });
    */

    $("#btn_all_signals").on("click",function(){
        _Fecha_Signal = "All";
        get_signals(_Fecha_Signal);
    });

    $("#btn_current_signals").on("click",function(){
        _Fecha_Signal = "Now";
        get_signals(_Fecha_Signal);
    });

    $("#btn_add_signal").on("click",function(){

        $("#Modal_Signal_Label").text("Add new signal");
        $("#Modal_Signal").modal("show");
    });

    function get_signals(_Fecha)
    {
        $.post("assets/php/process.php",{Action:"Signals",Fecha:_Fecha},function(data){
            console.log(data);
            $("#table_signals").dataTable().fnDestroy();
            
            $("#table_signals tbody").html(data);
            var _Table_Signals = $('#table_signals').DataTable({
                "order": [[ 1, "desc" ]]
            });

            $("#table_signals tbody").on("click","tr",function(){

                var row = _Table_Signals.row(this).data();
                
                $("#Modal_Message_Body_Title").text("Signal selected: "+row[0]);
                $("#Modal_Message").modal("show");
            });
        });
    }
});