$(document).ready(function(){

    $('#txt_password').keyup(function(e){
        if(e.keyCode == 13)
        {
            $("#lnk_acceder").trigger("click");
        }
    });

    $("#lnk_acceder").on("click",function(){

        var _Usuario = $("#txt_usuario").val();
        var _Password = $("#txt_password").val();
        
        if(_Usuario == 'admin' && _Password == 'admin')
        {
            Cookies.set('usuario', _Usuario);
            Cookies.set('password', _Password);

            
            $.LoadingOverlaySetup({
				color           : "rgba(0, 0, 0, 0.8)",
				image           : "assets/img/ring.svg",
				maxSize         : "80px",
				minSize         : "20px",
				resizeInterval  : 0,
				size            : "50%"
			});
			// Show full page LoadingOverlay
			$.LoadingOverlay("show");

			// Hide it after 2 seconds
			setTimeout(function(){

				$.LoadingOverlay("hide");
                window.location.href = "index.html";
                
			}, 2000);
        }
        else
        {
            $('#Modal_Login').modal('show');
        }
        
    });
});